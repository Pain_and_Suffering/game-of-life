def Evaluate(grid):
    copy = grid.Clone()
    for i in xrange(Height):
        for j in xrange(Width):
            living = FindLiveNeighbours(grid, i, j)
            if (living == 2):
                pass
            elif (living == 3):
                copy[i,j] = True
            else:
                copy[i,j] = False
    return copy

def FindLiveNeighbours(grid, i, j):
    count = 0
    for m in xrange(-1,2):
        for n in xrange(-1,2):
            if not IsOut(i+m,j+n):
                count += int(grid[i+m,j+n] == True)
    return count - int(grid[i,j] == True)

def IsOut(x,y):
    return ((y < 0) or (y >= Height) or (x < 0) or (x >= Width))
