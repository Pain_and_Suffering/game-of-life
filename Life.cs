﻿using System;
using IronPython.Hosting;
using Microsoft.Scripting.Hosting;

namespace Life
{
    public class Life
    {
        public Life(int h, int w)
        {
            if (h <= 0 || w <= 0)
                throw new ArgumentOutOfRangeException("Dimensions must be positive!");

            Height = h;
            Width = w;
            Ticks = 0;
            Grid = new bool[h, w];
        }

        public int Height { get; private set; }
        public int Width { get; private set; }
        public int Ticks { get; set; }
        private bool[,] Grid { get; set; }

        public bool this[int y, int x]
        {
            get
            {
                if (IsOutOfBounds(y, x))
                    throw new ArgumentOutOfRangeException("Coordinates must be non-negative!");

                return Grid[y, x];
            }

            set
            {
                if (IsOutOfBounds(y, x))
                    throw new ArgumentOutOfRangeException("Coordinates must be non-negative!");

                Grid[y, x] = value;
            }
        }

        public static ScriptEngine engine_py = Python.CreateEngine();
        public ScriptScope scope = engine_py.CreateScope();
            
        public void Tick()
        {
            scope.SetVariable("Height", Height);
            scope.SetVariable("Width", Width);

            engine_py.ExecuteFile("Backend.py", scope);
            dynamic Evaluate = scope.GetVariable("Evaluate");
            Grid = Evaluate(Grid);
            
            ++Ticks;
        }

        private bool IsOutOfBounds(int y, int x)
        {
            return y < 0 || y >= Height || x < 0 || x >= Width;
        }
    }
}